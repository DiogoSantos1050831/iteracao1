﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente1.Models;
using Microsoft.AspNetCore.Authorization;

namespace Semente1.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Posologias")]
    public class PosologiasController : Controller
    {
        private readonly Semente1Context _context;

        public PosologiasController(Semente1Context context)
        {
            _context = context;
        }

        // GET: api/Posologias
        [HttpGet]
        public IEnumerable<Posologia> GetPosologia()
        {
            return _context.Posologia;
        }

        // GET: api/Posologias/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPosologia([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologia = await _context.Posologia.SingleOrDefaultAsync(m => m.Id == id);

            if (posologia == null)
            {
                return NotFound();
            }

            return Ok(posologia);
        }

        // PUT: api/Posologias/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPosologia([FromRoute] int id, [FromBody] Posologia posologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != posologia.Id)
            {
                return BadRequest();
            }

            _context.Entry(posologia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PosologiaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Posologias
        [HttpPost]
        public async Task<IActionResult> PostPosologia([FromBody] Posologia posologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Posologia.Add(posologia);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosologia", new { id = posologia.Id }, posologia);
        }

        // DELETE: api/Posologias/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePosologia([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologia = await _context.Posologia.SingleOrDefaultAsync(m => m.Id == id);
            if (posologia == null)
            {
                return NotFound();
            }

            _context.Posologia.Remove(posologia);
            await _context.SaveChangesAsync();

            return Ok(posologia);
        }        

        private bool PosologiaExists(int id)
        {
            return _context.Posologia.Any(e => e.Id == id);
        }

        // GET: api/PosologiaMedicamento/
        [HttpGet("~/api/PosologiaMedicamento/List")]
        public IEnumerable<PosologiaMedicamento> ListPosologiaMedicamento()
        {
            return _context.PosologiaMedicamento;
        }

        // GET: api/PosologiaMedicamento/5
        [HttpGet("~/api/PosologiaMedicamento/{id:int}")]
        public async Task<IActionResult> GetPosologiaMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologiaMedicamento = await _context.PosologiaMedicamento.SingleOrDefaultAsync(m => m.Id == id);

            if (posologiaMedicamento == null)
            {
                return NotFound();
            }

            return Ok(posologiaMedicamento);
        }

        // PUT: api/PosologiaMedicamento/5
        [HttpPut("~/api/PosologiaMedicamento/{id:int}")]
        public async Task<IActionResult> PutPosologiaMedicamento([FromRoute] int id, [FromBody] PosologiaMedicamento posologiaMedicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != posologiaMedicamento.Id)
            {
                return BadRequest();
            }

            _context.Entry(posologiaMedicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PosologiaMedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PosologiaMedicamento
        [HttpPost, Route("~/api/PosologiaMedicamento")]
        public async Task<IActionResult> PostPosologiaMedicamento([FromBody] PosologiaMedicamento posologiaMedicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _context.PosologiaMedicamento.Add(posologiaMedicamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosologiaMedicamento", new { id = posologiaMedicamento.Id }, posologiaMedicamento);
        }

        // DELETE: api/PosologiaMedicamento/5
        [HttpDelete("~/api/PosologiaMedicamento/{id:int}")]
        public async Task<IActionResult> DeletePosologiaMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologiaMedicamento = await _context.PosologiaMedicamento.SingleOrDefaultAsync(m => m.Id == id);
            if (posologiaMedicamento == null)
            {
                return NotFound();
            }

            _context.PosologiaMedicamento.Remove(posologiaMedicamento);
            await _context.SaveChangesAsync();

            return Ok(posologiaMedicamento);
        }

        private bool PosologiaMedicamentoExists(int id)
        {
            return _context.PosologiaMedicamento.Any(e => e.Id == id);
        }

        // GET: api/PosologiaFarmaco/
        [HttpGet("~/api/PosologiaFarmaco/List")]
        public IEnumerable<PosologiaFarmaco> ListPosologiaFarmaco()
        {
            return _context.PosologiaFarmaco;
        }

        // GET: api/PosologiaFarmaco/5
        [HttpGet("~/api/PosologiaFarmaco/{id:int}")]
        public async Task<IActionResult> GetPosologiaFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologiaFarmaco = await _context.PosologiaFarmaco.SingleOrDefaultAsync(m => m.Id == id);

            if (posologiaFarmaco == null)
            {
                return NotFound();
            }

            return Ok(posologiaFarmaco);
        }

        // PUT: api/PosologiaFarmaco/5
        [HttpPut("~/api/PosologiaFarmaco/{id:int}")]
        public async Task<IActionResult> PutPosologiaFarmaco([FromRoute] int id, [FromBody] PosologiaFarmaco posologiaFarmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != posologiaFarmaco.Id)
            {
                return BadRequest();
            }

            _context.Entry(posologiaFarmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PosologiaFarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PosologiaFarmaco
        [HttpPost, Route("~/api/PosologiaFarmaco")]
        public async Task<IActionResult> PostPosologiaFarmaco([FromBody] PosologiaFarmaco posologiaFarmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _context.PosologiaFarmaco.Add(posologiaFarmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosologiaMedicamento", new { id = posologiaFarmaco.Id }, posologiaFarmaco);
        }

        // DELETE: api/PosologiaFarmaco/5
        [HttpDelete("~/api/PosologiaFarmaco/{id:int}")]
        public async Task<IActionResult> DeletePosologiaFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologiaFarmaco = await _context.PosologiaFarmaco.SingleOrDefaultAsync(m => m.Id == id);
            if (posologiaFarmaco == null)
            {
                return NotFound();
            }

            _context.PosologiaFarmaco.Remove(posologiaFarmaco);
            await _context.SaveChangesAsync();

            return Ok(posologiaFarmaco);
        }

        private bool PosologiaFarmacoExists(int id)
        {
            return _context.PosologiaFarmaco.Any(e => e.Id == id);
        }

        [HttpGet, Route("~/api/Medicamento/{id:int}/Posologias")]
        public async Task<IActionResult> GetMedicamentoPosologias([FromRoute] int id)
        {
            var apresent = await (from M in _context.PosologiaMedicamento
                                  .Include(M => M.Medicamento)
                                  where M.Medicamento.Id == id
                                  select M.PosologiaId).ToListAsync();
            if (apresent == null)
                return NotFound();
            return Ok(apresent);
        }


        [HttpGet, Route("~/api/Farmaco/{id:int}/Posologias")]
        public async Task<IActionResult> GetFarmacoPosologias([FromRoute] int id)
        {
            var apresent = await (from M in _context.PosologiaFarmaco
                                  .Include(M => M.Farmaco)
                                  where M.Farmaco.Id == id
                                  select M.PosologiaId).ToListAsync();
            if (apresent == null)
                return NotFound();
            return Ok(apresent);
        }


    }
}