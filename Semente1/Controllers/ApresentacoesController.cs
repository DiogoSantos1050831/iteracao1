﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente1.Models;
using Microsoft.AspNetCore.Authorization;
using Semente1.DTO;
using System.Linq.Expressions;

namespace Semente1.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Apresentacoes")]
    public class ApresentacoesController : Controller
    {
        private readonly Semente1Context _context;

        public ApresentacoesController(Semente1Context context)
        {
            _context = context;
        }

        // GET: api/Apresentacoes
        [HttpGet]
        public IEnumerable<Apresentacao> GetApresentacao()
        {
            return _context.Apresentacao;
        }

        //[HttpGet, Route("{id:int}/Apresentacoes2")]
        //public IQueryable<MedicamentoApresentacoesDTO> GetApresentacoes2(int id)
        //{
        //    return _context.ApresentacaoMedicamento.Include(m => m.Medicamento).Where(a => a.Medicamento.Id == id).Select(AsApresentacaoSelect);
        //}


        private static readonly Expression<Func<Apresentacao, ApresentacaoDTO>> AsApresentacaoSelect = x => new ApresentacaoDTO
        {
            Id = x.Id,
            Forma = x.Forma,
            Concentracao = x.Concentracao,
            Qtd = x.QTD
        };

        // GET: api/Apresentacoes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApresentacaoDTO apresentacao = await _context.Apresentacao.Select(AsApresentacaoSelect).SingleOrDefaultAsync(m => m.Id == id);

            if (apresentacao == null)
            {
                return NotFound();
            }

            return Ok(apresentacao);
        }

        // PUT: api/Apresentacoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApresentacao([FromRoute] int id, [FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(apresentacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Apresentacoes
        [HttpPost]
        public async Task<IActionResult> PostApresentacao([FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Apresentacao.Add(apresentacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApresentacao", new { id = apresentacao.Id }, apresentacao);
        }

        // DELETE: api/Apresentacoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            _context.Apresentacao.Remove(apresentacao);
            await _context.SaveChangesAsync();

            return Ok(apresentacao);
        }

        private bool ApresentacaoExists(int id)
        {
            return _context.Apresentacao.Any(e => e.Id == id);
        }

        // GET: api/ApresentacaoMedicamento/
        [HttpGet("~/api/ApresentacaoMedicamento/List")]
        public IEnumerable<ApresentacaoMedicamento> ListApresentacaoMedicamento()
        {
            return _context.ApresentacaoMedicamento;
        }

        // GET: api/ApresentacaoMedicamento/5
        [HttpGet("~/api/ApresentacaoMedicamento/{id:int}")]
        public async Task<IActionResult> GetApresentacaoMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacaoMedicamento = await _context.ApresentacaoMedicamento.SingleOrDefaultAsync(m => m.Id == id);

            if (apresentacaoMedicamento == null)
            {
                return NotFound();
            }

            return Ok(apresentacaoMedicamento);
        }

        // PUT: api/ApresentacaoMedicamento/5
        [HttpPut("~/api/ApresentacaoMedicamento/{id:int}")]
        public async Task<IActionResult> PutApresentacaoMedicamento([FromRoute] int id, [FromBody] ApresentacaoMedicamento apresentacaoMedicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacaoMedicamento.Id)
            {
                return BadRequest();
            }

            _context.Entry(apresentacaoMedicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoMedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApresentacaoMedicamento
        [HttpPost, Route("~/api/ApresentacaoMedicamento")]
        public async Task<IActionResult> PostApresentacaoMedicamento([FromBody] ApresentacaoMedicamento apresentacaoMedicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _context.ApresentacaoMedicamento.Add(apresentacaoMedicamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApresentacaoMedicamento", new { id = apresentacaoMedicamento.Id }, apresentacaoMedicamento);
        }

        // DELETE: api/ApresentacaoMedicamento/5
        [HttpDelete("~/api/ApresentacaoMedicamento/{id:int}")]
        public async Task<IActionResult> DeleteApresentacaoMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacaoMedicamento = await _context.ApresentacaoMedicamento.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacaoMedicamento == null)
            {
                return NotFound();
            }

            _context.ApresentacaoMedicamento.Remove(apresentacaoMedicamento);
            await _context.SaveChangesAsync();

            return Ok(apresentacaoMedicamento);
        }

        private bool ApresentacaoMedicamentoExists(int id)
        {
            return _context.ApresentacaoMedicamento.Any(e => e.Id == id);
        }

        // GET: api/ApresentacaoFarmaco/
        [HttpGet("~/api/ApresentacaoFarmaco/List")]
        public IEnumerable<ApresentacaoFarmaco> ListApresentacaoFarmaco()
        {
            return _context.ApresentacaoFarmaco;
        }

        // GET: api/ApresentacaoFarmaco/5
        [HttpGet("~/api/ApresentacaoFarmaco/{id:int}")]
        public async Task<IActionResult> GetApresentacaoFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacaoFarmaco = await _context.ApresentacaoFarmaco.SingleOrDefaultAsync(m => m.Id == id);

            if (apresentacaoFarmaco == null)
            {
                return NotFound();
            }

            return Ok(apresentacaoFarmaco);
        }

        // PUT: api/ApresentacaoFarmaco/5
        [HttpPut("~/api/ApresentacaoFarmaco/{id:int}")]
        public async Task<IActionResult> PutApresentacaoFarmaco([FromRoute] int id, [FromBody] ApresentacaoFarmaco apresentacaoFarmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacaoFarmaco.Id)
            {
                return BadRequest();
            }

            _context.Entry(apresentacaoFarmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoFarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ApresentacaoFarmaco
        [HttpPost, Route("~/api/ApresentacaoFarmaco")]
        public async Task<IActionResult> PostApresentacaoFarmaco([FromBody] ApresentacaoFarmaco apresentacaoFarmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _context.ApresentacaoFarmaco.Add(apresentacaoFarmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApresentacaoFarmaco", new { id = apresentacaoFarmaco.Id }, apresentacaoFarmaco);
        }

        // DELETE: api/ApresentacaoFarmaco/5
        [HttpDelete("~/api/ApresentacaoFarmaco/{id:int}")]
        public async Task<IActionResult> DeleteApresentacaoFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacaoFarmaco = await _context.ApresentacaoFarmaco.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacaoFarmaco == null)
            {
                return NotFound();
            }

            _context.ApresentacaoFarmaco.Remove(apresentacaoFarmaco);
            await _context.SaveChangesAsync();

            return Ok(apresentacaoFarmaco);
        }

        private bool ApresentacaoFarmacoExists(int id)
        {
            return _context.PosologiaFarmaco.Any(e => e.Id == id);
        }

        //[HttpGet, Route("{id:int}/Apresentacoes2")]
        //public IQueryable<MedicamentoApresentacoesDTO> GetApresentacoes2(int id)
        //{
        //    return _context.ApresentacaoMedicamento.Include(m => m.Medicamento).Where(a => a.Medicamento.Id == id).Select(AsApresentacaoSelect);
        //}


        //private static readonly Expression<Func<ApresentacaoMedicamento, MedicamentoApresentacoesDTO>> AsApresentacaoSelect = x => new MedicamentoApresentacoesDTO
        //{
        //    Id = x.Id
        //};

        [HttpGet, Route("~/api/Medicamento/{id:int}/Apresentacoes")]
        public async Task<IActionResult> GetMedicamentoApresentacoes([FromRoute] int id)
        {
            var apresent = await (from M in _context.ApresentacaoMedicamento
                                  .Include(M => M.Medicamento)
                                  where M.Medicamento.Id == id
                                  select M.ApresentacaoId).ToListAsync();
            if (apresent == null)
                return NotFound();
            return Ok(apresent);
        }


        [HttpGet, Route("~/api/Farmaco/{id:int}/Apresentacoes")]
        public async Task<IActionResult> GetFarmacoApresentacoes([FromRoute] int id)
        {
            var apresent = await (from M in _context.ApresentacaoFarmaco
                                  .Include(M => M.Farmaco)
                                  where M.Farmaco.Id == id
                                  select M.ApresentacaoId).ToListAsync();
            if (apresent == null)
                return NotFound();
            return Ok(apresent);
        }

        //[HttpGet, Route("~/api/Farmaco/{id:int}/Apresentacoes")]
        //public IQueryable<FarmacoApresentacoesDTO> GetFarmacoApresentacoes(int id)
        //{
        //    return _context.ApresentacaoFarmaco.Include(m => m.Farmaco).Where(a => a.Farmaco.Id == id).Select(AsFarmacoApresentacaoSelect);
        //}

        //private static readonly Expression<Func<ApresentacaoFarmaco, FarmacoApresentacoesDTO>> AsFarmacoApresentacaoSelect = x => new FarmacoApresentacoesDTO
        //{
        //    Id = x.Id
        //};
    }
}