﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente1.DTO
{
    public class ApresentacaoDTO
    {
        public int MyProperty { get; set; }
        public int Id { get; set; }
        public string Forma { get; set; }
        public string Concentracao { get; set; }
        public string Qtd { get; set; }
    }
}
