﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente1.DTO
{
    public class FarmacoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
