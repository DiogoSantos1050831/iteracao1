﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Semente1.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Semente1.Models
{
    public class Semente1Context : IdentityDbContext<UserEntity>
    {
        public Semente1Context (DbContextOptions<Semente1Context> options)
            : base(options)
        {
        }

        public DbSet<Semente1.Models.Medicamento> Medicamento { get; set; }

        public DbSet<Semente1.Models.Farmaco> Farmaco { get; set; }

        public DbSet<Semente1.Models.Apresentacao> Apresentacao { get; set; }

        public DbSet<Semente1.Models.Posologia> Posologia { get; set; }

        public DbSet<Semente1.Models.PosologiaFarmaco> PosologiaFarmaco { get; set; }

        public DbSet<Semente1.Models.PosologiaMedicamento> PosologiaMedicamento { get; set; }

        public DbSet<Semente1.Models.ApresentacaoFarmaco> ApresentacaoFarmaco { get; set; }
        public DbSet<Semente1.Models.ApresentacaoMedicamento> ApresentacaoMedicamento { get; set; }
    }
}
