﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente1.Models
{
    public class ApresentacaoFarmaco
    {
        public int Id { get; set; }
        public int ApresentacaoId { get; set; }
        public Apresentacao Apresentacao { get; set; }
        public int FarmacoId { get; set; }
        public Farmaco Farmaco { get; set; }
    }
}
