﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente1.Models
{
    public class PosologiaFarmaco
    {
        public int Id { get; set; }
        public int PosologiaId { get; set; }
        public Posologia Posologia { get; set; }
        public int FarmacoId { get; set; }
        public Farmaco Farmaco { get; set; }
    }
}
