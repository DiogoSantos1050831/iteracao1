﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente1.Models
{
    public class Apresentacao
    {
        public int Id { get; set; }
        
        public string Nome { get; set; }
     
        public string Forma { get; set; }
   
        public string Concentracao { get; set; }

        public string QTD { get; set; }

    }
}
