﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente1.Models
{
    public class ApresentacaoMedicamento
    {
        public int Id { get; set; }
        public int ApresentacaoId { get; set; }
        public Apresentacao Apresentacao { get; set; }
        public int MedicamentoId { get; set; }
        public Medicamento Medicamento { get; set; }

    }
}
